FROM python:3.10.5-slim-buster
WORKDIR /app
COPY cfg.yml .
COPY src src
COPY Pipfile* ./
RUN pip install pipenv
RUN rm src/test_*
RUN pipenv install --system --deploy
#ARG PORT
#ARG ENV
#ENV ENV=$ENV
#EXPOSE $PORT
CMD ["python", "-m", "src.mqtt"]
