# py-iot 🐍

![](https://img.shields.io/gitlab/pipeline-status/iot-craft/py-iot?branch=beta&logo=gitlab&style=for-the-badge)
![](https://img.shields.io/gitlab/coverage/iot-craft/py-iot/beta?logo=pytest&style=for-the-badge)
