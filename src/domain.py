from typing import List


def bitwise_and(children: List[str]) -> bool:
    return all([float(child) for child in children])


def average(children: List[str]) -> float:
    floats = [float(child) for child in children]
    return sum(floats) / len(floats)
