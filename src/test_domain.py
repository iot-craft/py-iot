from src.domain import bitwise_and, average


def test_bitwise_and() -> None:
    assert bitwise_and(["1", "2", "3.0"]) is True
    assert bitwise_and(["1.0000", "2", "0.0000"]) is False


def test_average() -> None:
    assert average(["1.00", "2.0", "3.0"]) == 2.0
